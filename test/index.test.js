"use strict";

const expect = require("chai").expect;
const { rakamOku } = require("../");

describe("index.js (Main Interface)", () => {

	describe(`Tamsayı Okumaları`, () => {
		describe("Sayı Okumak", () => {
			it("Boş boş döner", async () => {
				let result = await rakamOku("");
				expect(result).to.equal("Hata! Boş string.");
			});
			it("Normal bir tamsayı : 1", async () => {
				let result = await rakamOku("1");
				expect(result).to.equal("bir");
			});
			it("Normal bir tamsayı : 10", async () => {
				let result = await rakamOku("10");
				expect(result).to.equal("on");
			});
			it("Normal bir tamsayı : 100", async () => {
				let result = await rakamOku("100");
				expect(result).to.equal("yüz");
			});
			it("Normal bir tamsayı : 1234567890", async () => {
				let result = await rakamOku("1234567890");
				expect(result).to.equal("bir milyar ikiyüzotuzdört milyon beşyüzaltmışyedi bin sekizyüzdoksan");
			});
			it("Baştaki sıfırları görmezden gelir : 000001234567890", async () => {
				let result = await rakamOku("000001234567890");
				expect(result).to.equal("bir milyar ikiyüzotuzdört milyon beşyüzaltmışyedi bin sekizyüzdoksan");
			});
			it("Sadece sıfır boş döner : 0 > ''", async () => {
				let result = await rakamOku("0");
				expect(result).to.equal("sıfır");
			});
			it("1000 rakamını 'bir bin' diye okumaz : 1000 : bin", async () => {
				let result = await rakamOku("1000");
				expect(result).to.equal("bin");
			});
			it("Bir haneli rasgele rakam : 6", async () => {
				let result = await rakamOku("6");
				expect(result).to.equal("altı");
			});
			it("İki haneli rastgele rakam : 79", async () => {
				let result = await rakamOku("79");
				expect(result).to.equal("yetmişdokuz");
			});
			it("Üç haneli rastgele rakam : 284", async () => {
				let result = await rakamOku("284");
				expect(result).to.equal("ikiyüzseksendört");
			});
			it("Bir Milyon : 1000000", async () => {
				let result = await rakamOku("1000000");
				expect(result).to.equal("bir milyon");
			});
			it("Bir Milyon bin bir : 1001001", async () => {
				let result = await rakamOku("1001001");
				expect(result).to.equal("bir milyon bin bir");
			});
		});

		describe("Para Okumak", () => {
			it("Normal bir tamsayı : 1234567890", async () => {
				let result = await rakamOku("1234567890", "TRY");
				expect(result).to.equal("bir milyar ikiyüzotuzdört milyon beşyüzaltmışyedi bin sekizyüzdoksan lira");
			});
			it("Baştaki sıfırları görmezden gelir : 000001234567890", async () => {
				let result = await rakamOku("000001234567890", "TRY");
				expect(result).to.equal("bir milyar ikiyüzotuzdört milyon beşyüzaltmışyedi bin sekizyüzdoksan lira");
			});
			it("Sadece sıfır : sıfır lira > ''", async () => {
				let result = await rakamOku("0", "TRY");
				expect(result).to.equal("sıfır lira");
			});
			it("Sadece sıfır : sıfır euro > ''", async () => {
				let result = await rakamOku("0", "EUR");
				expect(result).to.equal("sıfır euro");
			});
			it("1000 rakamını 'bir bin' diye okumaz : 1000 > bin", async () => {
				let result = await rakamOku("1000", "TRY");
				expect(result).to.equal("bin lira");
			});
			it("Bir haneli rasgele rakam : 6", async () => {
				let result = await rakamOku("6", "TRY");
				expect(result).to.equal("altı lira");
			});
			it("İki haneli rastgele rakam : 79", async () => {
				let result = await rakamOku("79", "TRY");
				expect(result).to.equal("yetmişdokuz lira");
			});
			it("Üç haneli rastgele rakam : 284", async () => {
				let result = await rakamOku("284", "TRY");
				expect(result).to.equal("ikiyüzseksendört lira");
			});
			it("Bir Milyon : 1000000", async () => {
				let result = await rakamOku("1000000", "TRY");
				expect(result).to.equal("bir milyon lira");
			});
			it("Bir Milyon bin bir : 1001001", async () => {
				let result = await rakamOku("1001001", "TRY");
				expect(result).to.equal("bir milyon bin bir lira");
			});
		});
	});

	describe("Ondalık Okumaları", () => {
		describe("Sayı Okumak", () => {
			it("1.1", async () => {
				let result = await rakamOku("1.1");
				expect(result).to.equal("bir nokta bir");
			});
			it("1,1", async () => {
				let result = await rakamOku("1,1");
				expect(result).to.equal("bir virgül bir");
			});
			it("100.01", async () => {
				let result = await rakamOku("100.01");
				expect(result).to.equal("yüz nokta sıfır bir");
			});
			it("1000.001", async () => {
				let result = await rakamOku("1000.001");
				expect(result).to.equal("bin nokta sıfır sıfır bir");
			});
			it("1001001,010", async () => {
				let result = await rakamOku("1001001,010");
				expect(result).to.equal("bir milyon bin bir virgül sıfır bir");
			});
			it("0123456789.0123456789", async () => {
				let result = await rakamOku("0123456789.0123456789");
				expect(result).to.equal("yüzyirmiüç milyon dörtyüzellialtı bin yediyüzseksendokuz nokta sıfır yüzyirmiüç milyon dörtyüzellialtı bin yediyüzseksendokuz");
			});
		});
		describe("Para Okumak", () => {
			it("1.1", async () => {
				let result = await rakamOku("1.1", "TRY");
				expect(result).to.equal("bir lira on kuruş");
			});
			it("1.10", async () => {
				let result = await rakamOku("1.10", "TRY");
				expect(result).to.equal("bir lira on kuruş");
			});
			it("1.01", async () => {
				let result = await rakamOku("1.01", "TRY");
				expect(result).to.equal("bir lira bir kuruş");
			});
			it("00001.01", async () => {
				let result = await rakamOku("00001.01", "TRY");
				expect(result).to.equal("bir lira bir kuruş");
			});
		});
	});

	describe("Array Okumaları", () => {
		it("Tamsayı", async () => {
			let liste = ["3", "5", "8", "10"];
			let result = await rakamOku(liste);
			expect(result).to.eql(["üç", "beş", "sekiz", "on"]);
		});
		it("Ondalık", async () => {
			let liste = ["3.1", "5,01", "8.80", "01.010"];
			let result = await rakamOku(liste);
			expect(result).to.eql(["üç nokta bir",
				"beş virgül sıfır bir", "sekiz nokta sekiz",
				"bir nokta sıfır bir"]);
		});
		it("Para Tamsayı", async () => {
			let liste = ["3", "5", "8", "10"];
			let result = await rakamOku(liste, "EUR");
			expect(result).to.eql(["üç euro", "beş euro", "sekiz euro", "on euro"]);
		});
		it("para Ondalık", async () => {
			let liste = ["3.1", "5,01", "8.80", "01.010"];
			let result = await rakamOku(liste, "TRY");
			expect(result).to.eql(["üç lira on kuruş",
				"beş lira bir kuruş", "sekiz lira seksen kuruş",
				"bir lira bir kuruş"]);
		});
	});
	describe("Errorlar", () => {
		it("Boş error döner", async () => {
			let result = await rakamOku("");
			expect(result).to.equal("Hata! Boş string.");
		});
		it("Tamsayı > sağında boşluk varsa geçersizdir. ", async () => {
			let result = await rakamOku("123 ");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Tamasayı > ortasında boşluk varsa geçersizdir.", async () => {
			let result = await rakamOku("12 3");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Tamasayı > solunda boşluk varsa geçersizdir.", async () => {
			let result = await rakamOku(" 123");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Ondalık > sağında boşluk varsa geçersizdir.", async () => {
			let result = await rakamOku("123.23 ");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Ondalık > solunda boşluk varsa geçersizdir.", async () => {
			let result = await rakamOku(" 123,23");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Ondalık > tamsayı ortasında boşluk varsa geçersizdir.", async () => {
			let result = await rakamOku("12 3,23");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Ondalık > virgül sağında boşluk varsa geçersizdir.", async () => {
			let result = await rakamOku("123, 23");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Ondalık > ondalık ortasında boşluk varsa geçersizdir.", async () => {
			let result = await rakamOku("123,2 3");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Ondalık > ondalık ortasında boşluklar varsa geçersizdir.", async () => {
			let result = await rakamOku("123,2 0 3");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Ondalık > ondalık ortasında boşluklar varsa geçersizdir.", async () => {
			let result = await rakamOku("123,02 0");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Ondalık > ondalık ortasında boşluklar varsa geçersizdir.", async () => {
			let result = await rakamOku("123,020 ");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Para okuma > virgülden sonra 2 haneden fazla okumasın.", async () => {
			let result = await rakamOku("123,025", "TRY");
			expect(result).not.to.equal("yüzyirmiüç lira yirmibeş kuruş");
		});
		it("e harfi içeren number kabul etmesin.", async () => {
			let result = await rakamOku(123e5);
			expect(result).to.equal("Hata! Number vermeyiniz.");
		});
		it("e harfi içeren string kabul etmesin.", async () => {
			let result = await rakamOku("123e5");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("e harfi içeren string para kabul etmesin.", async () => {
			let result = await rakamOku("123e5", "EUR");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Geçersiz para birimi : ASD ", async () => {
			let result = await rakamOku("123", "ASD");
			expect(result).to.equal("Hata! Parakodu geçersiz.");
		});
		it("Space para birimi", async () => {
			let result = await rakamOku("123", " ");
			expect(result).to.equal("Hata! Parakodu geçersiz.");
		});
		it("Boş para birimi", async () => {
			let result = await rakamOku("123", "");
			expect(result).to.equal("Hata! Parakodu geçersiz.");
		});
		it("null para birimi", async () => {
			let result = await rakamOku("123", null);
			expect(result).to.equal("Hata! Parakodu geçersiz.");
		});
		it("undefined para birimi", async () => {
			let result = await rakamOku("123", undefined);
			expect(result).to.equal("yüzyirmiüç");
		});
		it("Geçersiz rakam : ASD ", async () => {
			let result = await rakamOku("asd");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Space tamsayı", async () => {
			let result = await rakamOku(" ");
			expect(result).to.equal("Hata! Geçersiz data.");
		});
		it("Boş tamsayı", async () => {
			let result = await rakamOku("");
			expect(result).to.equal("Hata! Boş string.");
		});
		it("null tamasyı", async () => {
			let result = await rakamOku(null);
			expect(result).to.equal("");
		});
		it("undefined tamsayı", async () => {
			let result = await rakamOku(undefined);
			expect(result).to.equal("");
		});
	});
});