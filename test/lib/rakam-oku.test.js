"use strict";

const expect = require("chai").expect;
const { rakamOkuInterface, paraBirimleriInterface } = require("../../lib/rakam-oku");



describe("rakam-oku.js (Class Interface)", () => {
	describe("Interface Import Checks", () => {
		it("Sıfır boş döner", async () => {
			let result = await rakamOkuInterface("0");
			expect(result).to.equal("sıfır");
		});
		it("Rakam Oku", async () => {
			let result = await rakamOkuInterface("1001001");
			expect(result).to.equal("bir milyon bin bir");
		});
		it("Rakam Oku", async () => {
			let result = await rakamOkuInterface("123123123");
			expect(result).to.equal("yüzyirmiüç milyon yüzyirmiüç bin yüzyirmiüç");
		});
		it("Para Oku", async () => {
			let result = await rakamOkuInterface("7", "TRY");
			expect(result).to.equal("yedi lira");
		});
		it("Para Oku", async () => {
			let result = await rakamOkuInterface("7.1", "TRY");
			expect(result).to.equal("yedi lira on kuruş");
		});
		it("Para Birimleri", async () => {
			let result = await paraBirimleriInterface["EUR"]["birim"];
			expect(result).to.include("euro");
		});
	});
});