"use strict";

const {
	rakamOkuInterface,
	paraBirimleriInterface } = require("./lib/rakam-oku");

module.exports = {
	rakamOku: rakamOkuInterface,
	paraBirimleri: paraBirimleriInterface
};