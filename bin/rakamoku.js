#!/usr/bin/env node
"use strict";

const { rakamOku } = require("../");

// ilk iki argüman path. siliyoruz.
const args = process.argv.slice(2);

// FONKSIYON TANIMLAMALARI
const getArg = (arg, liste) => {
	let argIndex = liste.indexOf(arg);
	return (argIndex === -1) ? null : liste[argIndex + 1];
};

const removeArgFromList = (arg, liste) => {
	let argIndex = liste.indexOf(arg);
	liste.splice(argIndex, 1); // delete param
	liste.splice(argIndex, 1); // delete value
	return liste;
};

// MAIN FONKSIYON
const oku = async (args) => {
	try {
		if (getArg("--para", args)) {
			const paraBirimi = getArg("--para", args);
			let sadeceRakamlar = removeArgFromList("--para", args);
			let result = await rakamOku(sadeceRakamlar, paraBirimi);
			if (result.length === 1) {
				return result.toString();
			}
			return result;
		} else {
			let result = await rakamOku(args);
			return result;
		}
	} catch (error) {
		return "Hata! CLI okuma başarısız!";
	}
};

// RUN
oku(args).then(console.log).catch(console.error);