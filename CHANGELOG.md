# Changelog
[Paketin] değişim detayları aşağıda belirtilmiştir.

## [0.0.1] - 2018.11.10
### Eklendi
- Çalışacak hale getirildi.
- Paket olarak yayına sunuldu.

## [Unreleased] - 2018.11.4
### Eklendi
- npm init.

[Paketin]: https://gitlab.com/ulvido/rakam-oku